export default () => {
  // 开发环境配置
  const config = {
    mysql: {
      host: 'localhost',
      port: 3306,
      username: 'nest_cms_api',
      password: 'nest_cms_api',
      database: 'nest_cms_api',
      synchronize: true,
    },
  };
  return config;
};
