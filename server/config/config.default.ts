import { merge } from 'lodash';
import local from './config.local';
import pord from './config.prod';
import unittest from './config.unittest';

// 默认配置 - 会自动合并运行环境配置。
export default () => {
  // 默认配置
  const config = {
    // 项目启动端口
    port: 3000,
  };
  return merge(config, { local, pord, unittest }[process.env.FM_SERVER_ENV]());
};
